function renderDSNV(dsnv){
    var contentHTML = "";
    for (var i=0; i<dsnv.length; i++){
        var nv = dsnv[i]
        var content = `<tr>
        <td>${nv.taiKhoanNv}</td>
        <td>${nv.tenNv}</td>
        <td>${nv.emailNv}</td>
        <td>${nv.ngayNvLam}</td>
        <td>${nv.chucVuNv}</td>
        <td>0</td>
        <td>${nv.xeploai()}</td>
        <td>
        <button class="btn btn-danger" onclick="xoaNhanVien('${nv.taiKhoanNv}')">Xóa</button>
        <button class="btn btn-warning" onclick="suaNhanVien('${nv.taiKhoanNv}')" data-target="#myModal" data-toggle="modal">Sửa</button>
        </td>
        
        </tr>`;
        
        contentHTML += content  
    }
    
    document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function showNhanVienLenForm(nv){
    document.getElementById("tknv").value = nv.taiKhoanNv;
    document.getElementById("name").value = nv.tenNv;
    document.getElementById("email").value = nv.emailNv;
    document.getElementById("password").value = nv.matKhauNv;
    document.getElementById("datepicker").value = nv.ngayNvLam;
    document.getElementById("luongCB").value = nv.luongCb;
    document.getElementById("chucvu").value = nv.chucVuNv;
    document.getElementById("gioLam").value = nv.gioLamNv;
}

function layThongTinTuForm(){
    var taiKhoanNv = document.getElementById("tknv").value;
    var tenNv= document.getElementById("name").value;
    var emailNv = document.getElementById("email").value;
    var matKhauNv = document.getElementById("password").value;
    var ngayNvLam = document.getElementById("datepicker").value;
    var luongCb = document.getElementById("luongCB").value *1;
    var chucVuNv = document.getElementById("chucvu").value;
    var gioLamNv = document.getElementById("gioLam").value *1;

    return new NhanVien(taiKhoanNv, tenNv, emailNv, matKhauNv, ngayNvLam, luongCb, chucVuNv, gioLamNv);
}
