var dsnv = [];

var dataJson = localStorage.getItem("DSNV");
if(dataJson != null){
    dsnv = JSON.parse(dataJson).map(function(item){
        return new NhanVien(
            item.taiKhoanNv,
            item.tenNv,
            item.emailNv,
            item.matKhauNv,
            item.ngayNvLam,
            item.luongCb,
            item.chucVuNv,
            item.gioLamNv,
            item.tongLuong);
    })
    renderDSNV(dsnv);
}


function themNhanVien(){
    // lấy dữ liệu từ form
    var nv = layThongTinTuForm();
    //validate kiểm tra tài khoản nhân viên
    var isValid = kiemTraTrung(nv.taiKhoanNv, dsnv) && kiemTraDoDai(4, 6, "tbTKNV", "Tài khoản phải từ 4 đến 6 ký tự", nv.taiKhoanNv);
    
    // kiểm tra email
    isValid = kiemTraEmail(nv.emailNv) && kiemTraTrungEmail(nv.emailNv, dsnv);
    
    
    if(isValid){
    // đẩy vào object
    dsnv.push(nv);

    //render dsnv
    renderDSNV(dsnv);

    // save dssv vào localStorage
    var dataJson = JSON.stringify(dsnv);
    localStorage.setItem("DSNV", dataJson);
    }
}

function xoaNhanVien(id){
    // console.log(id)
    var index = dsnv.findIndex(function(item){
        return item.taiKhoanNv == id;
    })
    dsnv.splice(index, 1);
    renderDSNV(dsnv);
    var dataJson = JSON.stringify(dsnv);
    localStorage.setItem("DSNV", dataJson);
}

function suaNhanVien(id){
    
    var index = dsnv.findIndex(function(item){
        return item.taiKhoanNv == id;
    });
    showNhanVienLenForm(dsnv[index]);

}

function capNhatNV(){
    // lấy thông tin cập nhật
    var nv = layThongTinTuForm();
    console.log("nv", nv)

    // tìm vị trí nhân viên trên form trong dsnv
    var indexNv = dsnv.findIndex(function(item){
        return item.taiKhoanNv == nv.taiKhoanNv;
    });
    if(indexNv != -1){
        // gán sinh viên trên form vào vị trí trong dsnv
        dsnv[indexNv] = nv;
    };
    var dataJson = JSON.stringify(dsnv);
    localStorage.setItem("DSNV", dataJson);

    renderDSNV(dsnv);
}



// tạo button = addEventListener khi có id
document
.getElementById("btnThemNV")
.addEventListener("click",themNhanVien )
