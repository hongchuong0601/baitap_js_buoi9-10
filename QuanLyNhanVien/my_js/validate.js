
function showMessage (tbTKNV, message){
    document.getElementById(tbTKNV).innerText = message
}
//kiểm tra ID
function kiemTraTrung(tkNv, dsnv){
    var index = dsnv.findIndex(function(item){
        return item.taiKhoanNv == tkNv;
    });

    if(index == -1){
        showMessage("tbTKNV","")
        return true;
    } else {
        showMessage("tbTKNV","Mã nhân viên đã tồn tại")
        return false;
    }  
}

function kiemTraDoDai(min, max, tbTKNV, message, value){
    var length = value.length;
    if(length >= min && length <= max){
        showMessage("tbTKNV", "");
        return true;
    } else {
        showMessage("tbTKNV", message);
        return false;
    }
}

// kiểm tra email
function kiemTraEmail(email){
    const re =
  /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  var isEmail = re.test(email);
  if(isEmail){
    showMessage("tbEmail","")
    return true;
  } else {
    showMessage("tbEmail","Email không hợp lệ")
    return false;
  }
}

function kiemTraTrungEmail(tkEmail, dsnv){
    var index = dsnv.findIndex(function(item){
        return item.emailNv == tkEmail;
    });

    if(index == -1){
        showMessage("tbEmail","")
        return true;
    } else {
        showMessage("tbEmail","Tài khoản email đã tồn tại")
        return false;
    }  
}